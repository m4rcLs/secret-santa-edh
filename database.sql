CREATE TABLE public."users" (
	id serial NOT NULL,
	"name" varchar NOT NULL,
	code varchar NOT NULL,
    rerolls smallint NOT NULL DEFAULT 2,

	CONSTRAINT users_pk PRIMARY KEY (id),
	CONSTRAINT users_code UNIQUE (code)
);
CREATE UNIQUE INDEX user_id_idx ON public."user" (id);
CREATE UNIQUE INDEX user_code_idx ON public."user" (code);
CREATE INDEX user_name_idx ON public."user" ("name");

CREATE TABLE public.cards (
	id serial NOT NULL,
	"name" varchar NOT NULL,
	scryfall_link varchar NULL,
	edhrec_link varchar NULL,
	image_link varchar NULL,
	CONSTRAINT cards_pk PRIMARY KEY (id),
	CONSTRAINT cards_name UNIQUE ("name")
);
CREATE UNIQUE INDEX cards_id_idx ON public.cards (id);
CREATE UNIQUE INDEX cards_name_idx ON public.cards ("name");




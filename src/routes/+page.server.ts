import { fail, json } from "@sveltejs/kit";

import {
  createNewUser,
  findUserByCode,
  type User,
  type Card,
  cardsForUser,
} from "$lib/server/db";
import type { PageServerLoad } from "./$types";

function getGreeting(): string {
  const possible = ["Hey", "Hi", "Hallo", "Moin", "Servus"];
  return possible[Math.floor(Math.random() * possible.length)];
}

export const load: PageServerLoad = async ({ params, cookies }) => {
  const code = cookies.get("user_code");
  let user: User | null = null;
  let cards: Card[] = [];

  if (code) {
    user = await findUserByCode(code);
  }

  if (user?.id) {
    cards = await cardsForUser(user?.id);
  }

  return { user, cards, greeting: getGreeting() };
};

export const actions = {
  default: async ({ request, cookies }) => {
    const data = await request.formData();
    const name = data.get("name")?.toString();
    const code = data.get("code")?.toString();

    if (name === "" && code === "") {
      console.error("No name or code provided");
      fail(400, { success: false, incomplete: true });
      return { success: false };
    }

    if (code && code !== "") {
      const user = await findUserByCode(code);

      if (!user) {
        console.error(`No user found with code ${code}`);
        fail(404, { success: false, code });
        return {
          success: false,
          error: "Es existiert kein Nutzer mit diesem Code!",
        };
      }

      cookies.set("user_code", code);
      return { success: true, user };
    }

    if (name) {
      const user = await createNewUser(name);
      if (!user) {
        fail(409, { name });
        return {
          success: false,
          error: "Nutzer konnte nicht erstellt werden!",
        };
      }

      if (user.code) {
        cookies.set("user_code", user.code);
      }

      return { success: true, user };
    }

    return { success: false };
  },
};

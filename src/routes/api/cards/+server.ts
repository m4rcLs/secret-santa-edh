import { findUserByCode, rerollCards } from "$lib/server/db/db.js";
import { json } from "@sveltejs/kit";

export async function POST({ cookies }) {
  const code = cookies.get("user_code");
  if (!code) {
    return json({ error: "No user code provided" }, { status: 403 });
  }

  const user = await findUserByCode(code);
  if (!user) {
    return json({ error: "No user found for provided code" }, { status: 404 });
  }
  if (!user?.rerolls || user?.rerolls <= 0) {
    return json({ error: "No rerolls left" }, { status: 409 });
  }

  const cards = await rerollCards(user);
  return json({ user, cards }, { status: 200 });
}

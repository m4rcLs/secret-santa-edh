import fs from "fs";
import { db, cards, type NewCard } from "$lib/server/db";
import { IMPORT_CARDS } from "$env/static/private";
import { building } from "$app/environment";

const setTimeoutPromise = (timeout: number) =>
  new Promise((resolve) => {
    setTimeout(resolve, timeout);
  });

async function fetchCard(cardName: string): Promise<NewCard | undefined> {
  const response = await fetch(
    "https://api.scryfall.com/cards/search?q=" + cardName
  );
  if (!response.ok) {
    console.error(`Error fetching ${cardName}: ${response.statusText}`);
    return;
  }
  let json: ScryfallCardResponse = await response.json();

  if (json.total_cards == 0) {
    console.error(`Did not find card named ${cardName}`);
    return;
  }

  if (json.total_cards > 1) {
    console.error(`Found more than one card for ${cardName}`);
    return;
  }

  const foundCard = json.data[0];
  return {
    name: foundCard.name,
    scryfallLink: foundCard.scryfall_uri,
    edhrecLink: foundCard.related_uris.edhrec,
    imageLink: foundCard.card_faces
      ? foundCard.card_faces[0].image_uris.normal
      : foundCard.image_uris.normal,
  };
}

async function fetchCards(
  cardNames: string[],
  i: number,
  existingCardNames: (string | null)[]
) {
  if (i > cardNames.length - 1) {
    return;
  }
  const cardName = cardNames[i];
  // We only fetch unknown cards
  if (existingCardNames.includes(cardName)) {
    console.log(`${cardName} already exists. Skipping ...`);
    await fetchCards(cardNames, i + 1, existingCardNames);
    return;
  }

  fetchCard(cardName).then((newCard) => {
    if (!newCard) {
      return;
    }
    db.insert(cards)
      .values(newCard)
      .then(() => {
        console.log(`Added card ${newCard.name}`);
      });
  });

  await setTimeoutPromise(150);
  await fetchCards(cardNames, i + 1, existingCardNames);
}

async function importCards() {
  const result = await db.select().from(cards);

  try {
    const data = fs.readFileSync("static/commanders.txt", "utf8");

    let cardNames = data.split("\n");
    let existingCards = result.map((card) => card.name);
    await fetchCards(cardNames, 0, existingCards);
  } catch (error) {
    console.error(error);
  }
  //   let newCards: NewCard[] = [];
}

if (IMPORT_CARDS && !building) {
  console.log("Importing cards");
  await importCards();
}

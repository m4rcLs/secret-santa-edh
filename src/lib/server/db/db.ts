import { POSTGRES_URL } from "$env/static/private";
import { users, type User, type NewUser, cards, type Card } from "./schema";
import { eq, sql } from "drizzle-orm";
import { drizzle } from "drizzle-orm/postgres-js";
import type { PostgresJsDatabase } from "drizzle-orm/postgres-js";
import postgres from "postgres";

const CODE_LENGTH = 6;

const client = postgres(POSTGRES_URL);
export const db: PostgresJsDatabase = drizzle(client);

export async function findUserByCode(code: string): Promise<User | null> {
  const user = await db.select().from(users).where(eq(users.code, code));

  return user[0] || null;
}

export async function cardsForUser(userId: number): Promise<Card[]> {
  return await db.select().from(cards).where(eq(cards.userId, userId));
}

function newCode(): string {
  let code = "";
  const possible = "0123456789";

  for (let i = 0; i < CODE_LENGTH; ++i) {
    code += possible.charAt(Math.floor(Math.random() * possible.length));
  }

  return code;
}

export async function createNewUser(name: string): Promise<User | null> {
  const newUser: NewUser = {
    name,
    code: newCode(),
  };

  try {
    const user = await db.insert(users).values(newUser).returning();
    await rollCards(user[0].id);

    return user[0];
  } catch (error: any) {
    console.error(error);
    return null;
  }
}

async function rollCards(userId: number): Promise<Card[]> {
  const cardResult = await db
    .select()
    .from(cards)
    .where(sql`${cards.userId} IS NULL`);
  const rolledCardIds: number[] = [];
  //   3 cards per user
  if (cardResult.length > 3) {
    for (let i = 0; i < 3; ++i) {
      const randomCard =
        cardResult[Math.floor(Math.random() * cardResult.length)];
      if (rolledCardIds.includes(randomCard.id)) {
        i--;
        continue;
      }
      rolledCardIds.push(randomCard.id);

      await db
        .update(cards)
        .set({ userId: userId })
        .where(eq(cards.id, randomCard.id));
    }
  }

  return await db.select().from(cards).where(eq(cards.userId, userId));
}

export async function rerollCards(user: User): Promise<Card[]> {
  if (!user.rerolls || user.rerolls <= 0) {
    console.error("User has no rerolls");
    return [];
  }
  await db.update(cards).set({ userId: null }).where(eq(cards.userId, user.id));
  const newCards = rollCards(user.id);

  await db
    .update(users)
    .set({ rerolls: user.rerolls - 1 })
    .where(eq(users.id, user.id));
  return newCards;
}

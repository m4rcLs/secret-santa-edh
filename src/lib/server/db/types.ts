type ScryfallImageUris = {
  small: string;
  normal: string;
  large: string;
  png: string;
  art_crop: string;
  border_crop: string;
};

type ScryfallPrices = {
  eur: number;
  eur_foil: number;
};

type ScryfallRelatedUris = {
  edhrec: string;
};

type ScryfallPurchaseUris = {
  cardmarket: string;
};

type ScryfallCardFace = {
  name: string;
  image_uris: ScryfallImageUris;
};

type ScryfallCard = {
  object: string;
  id: string;
  cardmarket_id: number;
  name: string;
  lang: string;
  scryfall_uri: string;
  image_uris: ScryfallImageUris;
  prices: ScryfallPrices;
  related_uris: ScryfallRelatedUris;
  purchase_uris: ScryfallPurchaseUris;
  card_faces: ScryfallCardFace[];
};

type ScryfallCardResponse = {
  object: string;
  total_cards: number;
  has_more: boolean;
  data: ScryfallCard[];
};

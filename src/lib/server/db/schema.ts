import {
  pgTable,
  serial,
  varchar,
  smallint,
  integer,
} from "drizzle-orm/pg-core";

import { relations } from "drizzle-orm";

export const users = pgTable("users", {
  id: integer("id").primaryKey(),
  name: varchar("name").notNull(),
  code: varchar("code").notNull().unique(),
  rerolls: smallint("rerolls"),
});

export const usersRelations = relations(users, ({ many }) => ({
  cards: many(cards),
}));

export type User = typeof users.$inferSelect; // return type when queried
export type NewUser = typeof users.$inferInsert;

export const cards = pgTable("cards", {
  id: serial("id").primaryKey(),
  name: varchar("name").notNull().unique(),
  scryfallLink: varchar("scryfall_link"),
  edhrecLink: varchar("edhrec_link"),
  imageLink: varchar("image_link"),
  userId: integer("user_id").references(() => users.id),
});

export const cardsRelations = relations(cards, ({ one }) => ({
  owner: one(users, { fields: [cards.userId], references: [users.id] }),
}));

export type Card = typeof cards.$inferSelect; // return type when queried
export type NewCard = typeof cards.$inferInsert;

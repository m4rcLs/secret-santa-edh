import { writable, type Writable } from "svelte/store";
import type { ActionData } from "../../routes/$types";

export const loginForm: Writable<ActionData | null> = writable(null);
